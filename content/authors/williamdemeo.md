+++
title = "William DeMeo"
description = "Creator of Thmpr and agda-algebras."
date = 2021-04-01T08:50:45+00:00
updated = 2021-04-01T08:50:45+00:00
draft = false
+++

Founder of [Thmpr](https://thmpr.org) and creator of [agda-algebras](https://ualib.org).

[@williamdemeo](https://github.com/williamdemeo)
