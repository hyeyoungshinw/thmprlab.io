+++
title = "Quick Start"
description = "One page summary of how to install the agda-algebras library."
date = 2021-05-01T08:20:00+00:00
updated = 2021-05-01T08:20:00+00:00
draft = false
weight = 20
sort_by = "weight"
template = "docs/page.html"

[extra]
lead = "One page summary of how to install the agda-algebras library."
toc = true
top = false
+++

## Requirements

Before using the library, you need to install [Agda](https://www.getzola.org/documentation/getting-started/installation/) ≥ 2.6.2,
and the [Agda Standard Library](https://github.com/agda/agda-stdlib) ≥ 1.7.

### Installing Agda and the Agda Standard Library

Agda ([version 2.6.2](https://agda.readthedocs.io/en/v2.6.1/getting-started/installation.html) or greater) is required. If you don't have it, follow the [official Agda installation instructions](https://agda.readthedocs.io/en/v2.6.2/getting-started/installation.html).

For reference, the following is a list of commands that should Agda version 2.6.2 on a Linux machine (with Ubuntu 20.04, e.g.). If problems arise when attempting to follow these steps, please 
[email the development team](mailto:williamdemeo@gmail.com) or submit a new issue to the
[git repo](https://github.com/ualib/agda-algebras).


```
cabal update
git clone git@github.com:agda/agda.git
cd agda
git checkout release-2.6.2
cabal install Agda-2.6.2 --program-suffix=-2.6.2  # (takes a very long time)
cd ~/.cabal/bin/
touch ~/.emacs
cp ~/.emacs ~/.emacs.backup
./agda-mode-2.6.2 setup
./agda-mode-2.6.2 compile
mkdir -p ~/bin
cp ~/.emacs ~/bin
cp ~/.emacs.backup ~/.emacs
cd ~/bin
echo '#!/bin/bash' > agdamacs
echo 'PATH=~/.cabal/bin:$PATH emacs --no-init-file --load ~/bin/.emacs \$@' >> agdamacs
chmod +x agdamacs
echo 'export PATH=~/bin:~/.cabal/bin:$PATH' >> ~/.profile
```

Now invoking the command `agdamacs` will launch emacs with Agda 2.6.2 and agda-mode installed.)


