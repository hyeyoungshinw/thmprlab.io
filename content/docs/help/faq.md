+++
title = "FAQ"
description = "Answers to frequently asked questions."
date = 2021-05-01T19:30:00+00:00
updated = 2021-05-01T19:30:00+00:00
draft = false
weight = 30
sort_by = "weight"
template = "docs/page.html"

[extra]
lead = "Answers to frequently asked questions."
toc = true
top = false
+++

## What is Thmpr?

Come back in January 2022... and the mystery will be revealed.

## Other documentation?

- [agda-algebras](https://ualib.org) The Agda Universal Algebra Library, Ver. 2.0
- [UALib](https://ualib.gitlab.io) The Agda Universal Algebra Library, Ver. 1.0
- [Agda Standard Library](https://github.com/agda/agda-stdlib)
- [Agda Tools](https://agda.readthedocs.io/en/v2.6.2/tools/index.html)
- [Agda Language Reference](https://agda.readthedocs.io/en/v2.6.2/language/index.html)


## Contact the creator?

Send *William DeMeo* an E-mail:

- <williamdemeo@gmail.com>
